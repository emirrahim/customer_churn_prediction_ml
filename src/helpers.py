# v1
# import numpy as np
# from collections import Counter

# v2
from src import np, Counter


# additional function

""" 
        int32 number => text in English alphabet
"""


def number2text(num):
    d = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five',
         6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten',
         11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen',
         15: 'fifteen', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen',
         19: 'nineteen', 20: 'twenty',
         30: 'thirty', 40: 'forty', 50: 'fifty', 60: 'sixty',
         70: 'seventy', 80: 'eighty', 90: 'ninety'}
    k = 1000
    m = k * 1000
    b = m * 1000
    t = b * 1000

    assert(0 <= num)

    if (num < 20):
        return d[num]

    if (num < 100):
        if num % 10 == 0:
            return d[num]
        else:
            return d[num // 10 * 10] + '-' + d[num % 10]

    if (num < k):
        if num % 100 == 0:
            return d[num // 100] + ' hundred'
        else:
            return d[num // 100] + ' hundred and ' + number2text(num % 100)

    if (num < m):
        if num % k == 0:
            return number2text(num // k) + ' thousand'
        else:
            return number2text(num // k) + ' thousand, ' + number2text(num % k)

    if (num < b):
        if (num % m) == 0:
            return number2text(num // m) + ' million'
        else:
            return number2text(num // m) + ' million, ' + number2text(num % m)

    if (num < t):
        if (num % b) == 0:
            return number2text(num // b) + ' billion'
        else:
            return number2text(num // b) + ' billion, ' + number2text(num % b)

    if (num % t == 0):
        return number2text(num // t) + ' trillion'
    else:
        return number2text(num // t) + ' trillion, ' + number2text(num % t)

    raise AssertionError('num is too large: %s' % str(num))


def separete_word(w):
    # additional function for separating words (2)

    words = [char for char in w if char.isupper()]

    second_word_first_letter = ''
    if(len(words) > 1):
        second_word_first_letter = words[1]
    else:
        return w

    words_list = w.split(second_word_first_letter)

    word_res = words_list[0] + " " + second_word_first_letter + words_list[1]

    # print(word_res)
    return word_res


#  Checking for outliers - Inter Quartile Range (IQR)
outliers = []


def outliers_iqr(df, n, features):
    for col in features:
        q1 = np.percentile(df[col], 25)
        # q2 = np.percentile(df[col], 50) # median
        q3 = np.percentile(df[col], 75)

        iqr = q3 - q1  # IQR

        outliers_list_cols = df[
            (df[col] > q3 + (iqr * 1.5)) | (df[col] < q1 - (iqr * 1.5))
        ].index

        # extend adds new list values to existing list # instead append add new list itself to existing list
        outliers.extend(outliers_list_cols)

    # selecting observations containing more than x outliers
    outlier_list = Counter(outliers)
    outliers_cnt = list(k for k, v in outlier_list.items() if v > n)
    # all_outliers

    lower_b_out = df[df[col] < q1 - (iqr * 1.5)]
    upper_b_out = df[df[col] > q3 + (iqr * 1.5)]
    total_outliers_shape = lower_b_out.shape[0] + upper_b_out.shape[0]

    print('Total number of outliers is:', total_outliers_shape)

    # return outliers_cnt


# even if we drop the outliers there will nothing be changed in the dataset
# all_outliers = get_outliers_iqr(df_eda, 1, (numerical_cols))
# df_out = df_eda.drop(all_outliers, axis=0).reset_index(drop=True)
# return df_out


def remove_txt_from_features(txt, features):
    updated_features_list = []

    for i, f in enumerate(features):
        if(txt in f):
            t_txt = f.replace(txt, '')
            updated_features_list.append(t_txt)
            # print(updated_features_list)

            if(len(t_txt.split(' ')) > 1):
                # print(t_txt)
                # print(updated_features_list.index(t_txt))
                txt_id = updated_features_list.index(t_txt)

                updated_features_list.pop(txt_id)
                updated_features_list.insert(
                    txt_id,
                    t_txt.replace(' ', "_").title()
                ),
        else:
            updated_features_list.append(f)

    # print(updated_features_list)
    return updated_features_list

