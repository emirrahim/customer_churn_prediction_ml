# Visualization functions

# v1
# import numpy as np
# import matplotlib as plt
# import pandas as pd
# import seaborn as sns

# v2
from src import pd, plt, sns

# import numberToText as n2t
from src.helpers import number2text as n2t
from src.helpers import separete_word


# Plot colors palette

palette = ['#008080', '#FF6347', '#E50000', '#D2691E']
palette2 = ['#FF6347', '#008080', '#E50000', '#D2691E']


# Histogram visualization technique for categoric data distribution

def categoric_hist_visualization(data, rows, cols, cols_list):
    fig, axs = plt.subplots(
        rows,
        cols,
        figsize=(20, 15)
    )

    fig.suptitle('Categorical Features Distribution', size=20)

    for id, col in enumerate(cols_list):
        # plt.subplot(int(np.ceil(id / 3)), int(id % 3), id)

        sns.histplot(
            data=data[col],
            ax=axs[id // cols][id % cols],
            # bin size  //  show how many data on each bin size (y axis - count)
            # bins=2,
            # kde=True,
            # kde_kws=s{'bw_method': 0.1},
            color=palette[0],
        )

        axs[id // cols][id % cols].set_title(f'Dist. of {col}')


# Kde visualization technique for numeric data distribution

def numeric_kde_visualization(data, cols_list, cols_c, rows_c, supTitle):
    fig, axs = plt.subplots(
        # data=data,
        nrows=rows_c,
        ncols=cols_c,
        figsize=(16, 6)
    )
    # seting figure title
    fig.suptitle(supTitle, size=20)
    # plot for each given column in data
    for i, col in enumerate(cols_list):
        sns.kdeplot(
            data=data[col],
            ax=axs[i],
            fill=True,  # fill inside
            alpha=0.8,  # transparency level
            linewidth=0,
            color='#008080'
        )
        axs[i].set_title(
            f'{col} skewness: ' + str(
                round((data[col].skew(axis=0, skipna=True)), 2)
            ))


# Box-plot visualization technique

def numeric_boxplot_distribution(data, cols_list, cols_c, rows_c, supTitle):
    fig, axs = plt.subplots(
        # data=data,
        nrows=rows_c,
        ncols=cols_c,
        # sharey=True, # share y axis ticks
        figsize=(16, 6)
    )

    # seting figure title
    fig.suptitle(supTitle, size=20)

    # plot for each given column in data
    for i, col in enumerate(cols_list):
        sns.boxplot(
            data=data[col],
            ax=axs[i],
            # orient='h',
            palette=palette
        )

        axs[i].set_title(
            f'{col} skewness: ' + str(
                round((data[col].skew(axis=0, skipna=True)), 2)
            ))


def distribution2(data, cols_list, cols_c, rows_c, supTitle):
    figs, axss = plt.subplots(
        # data=data,
        nrows=rows_c,
        ncols=cols_c,
        figsize=(16, 6)
    )

    # seting figure title
    figs.suptitle(supTitle, size=20)

    # plot for each given column in data
    for i, col in enumerate(cols_list):
        sns.kdeplot(
            data=data[col],
            ax=axss[i],
            fill=True,  # fill inside
            alpha=0.8,  # transparency level
            linewidth=0,
            color='#008080'
        )

        axss[i].set_title(
            f'{col} skewness: ' + str(
                round((data[col].skew(axis=0, skipna=True)), 2)
            ))


# Visualization for each period of contract

def visualize_by_contract(df, rows, cols, group_count, param='', selected_col='', bins=50, font_size=14, month_mean='', one_year_mean='', two_year_mean=''):
    # def visualize_by_contract(df, rows, cols, group_count, param='', selected_col='', bins=50, font_size=14):
    fig = plt.subplots(1, 3, figsize=(20, 5))

    for i in range(0, group_count):
        txt_en = n2t(i)
        print(txt_en)

        period_txt = "month_mean" if i == 0 else txt_en + "_year_mean"

        plt.subplot(rows, cols, i+1)
        ax = sns.histplot(
            df[df[param] == i][selected_col],
            bins=50,
            # palette=palette[0], // used with hue
            color=palette[0],
        )

        ax.legend(
            [selected_col.capitalize()],
            # by using eval function we can evaluate the text as parameter
            title=f'mean: {eval(period_txt)}',
            loc="upper left"
        )
        ax.set_title(
            f'{period_txt} year Contract Churn',
            size=14  # we can change it to dynamic way also
        )
        ax.set_xlabel(selected_col.title())
        ax.set_ylabel('No. of Customers')

    plt.show()


def numeric_fe_scatter_vis(df, rows, cols, words, hue='Churn'):

    fig = plt.figure(figsize=(20, 5))

    for i, word in enumerate(words):
        # print(words[i], words[i-1])

        plt.subplot(rows, cols, i+1)
        ax = sns.scatterplot(
            # if we define our data frame on beginning then it's not necessary to define it in all parameters again (for example instead of df1['Churn'] we can define as 'Churn')
            data=df,
            x=words[i],
            y=words[i-1],
            hue=hue,
            palette=palette[0:2],
            alpha=0.8,  # alpha => opacity of values in scatterplot
            # s => size
            s=10
        )

        plt.title(separete_word(words[i]) +
                  " vs " + separete_word(words[i-1]).capitalize())
        # plt.legend(['No', 'Yes'])
        ax.set_xlabel(separete_word(words[i]).capitalize())
        ax.set_ylabel(separete_word(words[i-1]).capitalize())

    plt.show()


# countplots visualisation
# v1
def countplots_vis(dataset, columns_list, rows, cols, figsize, suptitle, hue, palette):
    fig, axs = plt.subplots(rows, cols, sharey=True, figsize=figsize)

    fig.suptitle(suptitle, y=1, size=30)

    for i, data in enumerate(columns_list):
        ax = sns.countplot(
            data=dataset,
            ax=axs[i],
            x=columns_list[i],
            hue=hue,
            palette=palette,
            edgecolor='black')

        axs[i].set_title(data + f' vs {hue}', size=25)

        # each bar label in axis
        for i in ax.containers:
            ax.bar_label(i, size=14)
            ax.set_xlabel('')


# another version of countplot visualization
# v2
def countplots_vis2(dataset, columns_list, rows, cols, figsize, suptitle='', hue='', palette=''):
    fig = plt.subplots(rows, cols, sharey=True, figsize=figsize)

    # fig.suptitle(suptitle, y=1, size=30)

    for i, data in enumerate(columns_list):
        plt.subplot(rows, cols, i+1)

        ax = sns.countplot(
            data=dataset,
            # ax=axs[i],
            x=columns_list[i],
            hue=hue,
            palette=palette,
            edgecolor='black')

        ax.set_title(data + f' vs {hue}', size=20)

        # each bar label in axis
        for i in ax.containers:
            ax.bar_label(i, size=14)


# dynamic pie chart visualization

def pie_chart_vis(df, rows, cols, pie_values_list, cols_list, figsize=(20, 20)):
    fig = plt.figure(figsize=figsize)

    for i, v in enumerate(pie_values_list):
        # print(pie_values_list[i])

        explode = [0.1 if every > 0 else 0 for every in range(
            len(pie_values_list[i]))]
        explode = tuple(reversed(explode))
        # print(explode)

        labels = df[cols_list[i]].unique()
        # print(labels)

        plt.subplot(rows, cols, i+1)
        plt.pie(pie_values_list[i],
                labels=labels,
                autopct='%1.2f%%',
                startangle=90,
                explode=explode,
                colors=palette,
                wedgeprops={'edgecolor': 'green', 'linewidth': 1, 'antialiased': True})

        plt.title(separete_word(cols_list[i]), size=14)

    plt.tight_layout()
    plt.show()
