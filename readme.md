
# Customer Churn Prediction


# Classification (Supervised learning ML algorithm)


Getting Data,

Data Preparation/Pre-processing (data cleansing, data transformation),

Exploratory Data Analysis (EDA),

Data Visualization (matplotlib pyplot, seaborn),

Data Interpretation,

Data Balancing,

UnderSampling/Oversampling(bias, variance) w SMOTE, RandomUnderSampler, 
etc.,

Model Fitting,

Feature Engineering,

Feature Selection,

Machine Learning (ML) (bagging-Random Forest, boosting-XG Boost/Cross gradient 
boosting, voting),

Hyperparameter tuning,

Cross-Validation (K-Fold, GridSearch),

Metrics (recall, precision, f1 score, accuracy) roc_auc,

Model Performance Evaluation,

etc.


P.S. Some additional codes are written for educational purposes.
